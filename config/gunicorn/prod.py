"""Gunicorn *production* config file"""

wsgi_app = "eldritch_chronicle:app"
workers = 3
bind = "0.0.0.0:5000"
accesslog = "/var/log/gunicorn/access.log"
errorlog = "/var/log/gunicorn/error.log"
capture_output = True
pidfile = "/var/run/gunicorn/prod.pid"
daemon = True