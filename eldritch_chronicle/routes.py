import os

from flask import render_template, url_for, flash, redirect, request
from flask_login import login_user, current_user, logout_user, login_required

from db_seeding import seed_preludes, seed_ancients, download_ancient_images, download_investigators_images
from eldritch_chronicle import app, db, bcrypt
from eldritch_chronicle.campaign_manipulations import CampaignManipulation
from eldritch_chronicle.forms import RegistrationForm, LoginForm
from eldritch_chronicle.models import User, Campaign


@app.route("/")
@app.route("/home")
def home():
    return render_template('home.html')


@app.route("/seed_db_with_data")
def seed_db_with_data():
    if request.args.get('token') == os.getenv('DB_SEEDING_TOKEN'):
        seed_preludes()
        seed_ancients()
        download_ancient_images()
        download_investigators_images()
    return redirect(url_for('home'))


@app.route("/about")
def about():
    return render_template('about.html', title='About')


@app.route("/register", methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user = User(username=form.username.data, email=form.email.data, password=hashed_password)
        db.session.add(user)
        db.session.commit()
        flash('Your account has been created! You are now able to log in', 'success')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@app.route("/login", methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            next_page = request.args.get('next')
            return redirect(next_page) if next_page else redirect(url_for('home'))
        else:
            flash('Login Unsuccessful. Please check email and password', 'danger')
    return render_template('login.html', title='Login', form=form)


@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('home'))


@app.route("/account")
@login_required
def account():
    return render_template('account.html', title='Account')

@app.get("/campaigns")
@login_required
def campaigns_get():
    if not current_user.is_authenticated:
        return redirect(url_for('home'))
    campaigns_list = list(Campaign.query.filter_by(user_creator_id=current_user.id).order_by('id'))
    campaigns_list.reverse()
    return render_template('campaigns.html', campaigns=campaigns_list)

@app.post("/campaigns")
@login_required
def campaigns_post():
    request.form.get('test')
    campaign_api = CampaignManipulation()
    campaign_api.create_campaign(user_id=current_user.id)
    return redirect(url_for('campaign_by_id', campaign_id=campaign_api.campaign.id))


@app.route("/campaign/<campaign_id>", methods=['GET', 'POST'])
@login_required
def campaign_by_id(campaign_id):
    campaign_api = CampaignManipulation()
    campaign_api.get_campaign_by_id(campaign_id=campaign_id)
    if not current_user.id == campaign_api.campaign.user_creator_id:
        flash('This campaign was created by another user', 'warning')
        return redirect(url_for('home'))
    if request.method == 'POST':
        if request.form.get('action') == 'defeat':
            campaign_api.defeat_ancient()
        if request.form.get('action') == 'lose':
            campaign_api.lose_campaign()
    defeated_ancients = campaign_api.get_defeated_ancients()
    return render_template('campaign_by_id.html', campaign=campaign_api.campaign, defeated_ancients=defeated_ancients)
