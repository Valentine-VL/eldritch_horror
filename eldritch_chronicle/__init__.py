import os

from flask import Flask
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_talisman import Talisman

csp = {
    'default-src': [
        '\'self\'',
        'maxcdn.bootstrapcdn.com',
        'code.jquery.com',
        'cdnjs.cloudflare.com',
        'cdn.jsdelivr.net'
    ]
}

app = Flask(__name__,
            static_url_path=os.getenv('STATIC_PATH_DEV'),
            static_folder='static',
            template_folder='templates')
Talisman(app, content_security_policy=csp)

app.config.from_pyfile('settings.py')
app.config['SECRET_KEY'] = os.getenv('SECRET_KEY')
app.config['WTF_CSRF_SECRET_KEY'] = os.getenv('WTF_CSRF_SECRET_KEY')
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('SQLALCHEMY_DATABASE_URI')

db = SQLAlchemy(app)

bcrypt = Bcrypt(app)

login_manager = LoginManager(app)
login_manager.login_view = 'login'
login_manager.login_message_category = 'info'

from eldritch_chronicle import routes
