from eldritch_chronicle import db, login_manager
from flask_login import UserMixin
from datetime import datetime
from sqlalchemy.orm import relationship


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class User(db.Model, UserMixin):
    __table_args__ = dict(schema="ehct_app")
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(60), nullable=False)

    def __repr__(self):
        return f"User('{self.username}')"

class Prelude(db.Model):
    __table_args__ = dict(schema="ehct_app")
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True, nullable=False)
    basic_effect = db.Column(db.String(255), nullable=False)
    special_effect = db.Column(db.String(255), nullable=False)

    def __repr__(self):
        return f"Prelude('{self.name}')"

class Ancient(db.Model):
    __table_args__ = dict(schema="ehct_app")
    id = db.Column(db.Integer, primary_key=True)
    image_url = db.Column(db.String(255), nullable=True, default=None)
    name = db.Column(db.String(30), unique=True, nullable=False)

    prelude_id = db.Column(db.Integer, db.ForeignKey(Prelude.id), nullable=False)
    image_path = db.Column(db.String(255), nullable=True, default=None)
    # campaign_ancients_id = db.Column(db.Integer, db.ForeignKey('campaign_ancients.id'), nullable=True)

    prelude = relationship("Prelude", primaryjoin="and_(Prelude.id==Ancient.prelude_id)")


    def __repr__(self):
        return f"Ancient {self.name}"

class Investigator(db.Model):
    __table_args__ = dict(schema="ehct_app")
    id = db.Column(db.Integer, primary_key=True)
    image_file = db.Column(db.String(100), nullable=True, default=None)
    name = db.Column(db.String(50), unique=True, nullable=True)
    personal_quest_text = db.Column(db.String(500), nullable=False)
    personal_quest_success = db.Column(db.String(500), nullable=False)
    personal_quest_fail = db.Column(db.String(500), nullable=False)

    def __repr__(self):
        return f"Investigator {self.name}"

class City(db.Model):
    __table_args__ = dict(schema="ehct_app")
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), unique=True, nullable=False)


class Player(db.Model):
    __table_args__ = dict(schema="ehct_app")
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), unique=True, nullable=False)
    has_dark_pact = db.Column(db.Boolean, nullable=False, default=False)
    has_promise_power = db.Column(db.Boolean, nullable=False, default=False)
    investigator_id = db.Column(db.Integer, db.ForeignKey(Investigator.id), nullable=False)
    investigator = relationship("Investigator", primaryjoin="and_(Investigator.id==Player.investigator_id)")

    def __repr__(self):
        return f"Player('{self.name}')"

class Campaign(db.Model):
    __table_args__ = dict(schema="ehct_app")
    id = db.Column(db.Integer, primary_key=True)
    user_creator_id = db.Column(db.Integer, db.ForeignKey(User.id), nullable=False)
    player_ids = db.Column(db.String(20), nullable=True)
    destroyed_cities_ids = db.Column(db.String(255), nullable=True)
    dead_investigator_ids = db.Column(db.String(255), nullable=True)
    created_date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    finish_date = db.Column(db.DateTime, nullable=True)
    state = db.Column(db.String(20), default='in_progress')

    current_ancient_id = db.Column(db.Integer, db.ForeignKey(Ancient.id), nullable=True)
    next_ancient_id = db.Column(db.Integer, db.ForeignKey(Ancient.id), nullable=True)
    defeated_ancient_ids = db.Column(db.String(20), nullable=True)

    current_ancient = relationship("Ancient", primaryjoin="and_(Ancient.id==Campaign.current_ancient_id)")
    next_ancient = relationship("Ancient", primaryjoin="and_(Ancient.id==Campaign.next_ancient_id)")

