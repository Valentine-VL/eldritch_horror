import os
import requests
from bs4 import BeautifulSoup

from eldritch_chronicle.models import Prelude, Ancient, Investigator
from eldritch_chronicle import db

ANCIENTS_DICT = {"Abhoth": ["Epidemic", 11, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/4/46/Abhoth.png/'],
"Antediluvium": ["The Stars Align", 37, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/c/c4/Antediluvium.png/'],
"Atlach-Nacha": ["Web Between Worlds", 24, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/2/28/Atlach-Nacha.png'],
"Azathoth": ["Beginning of the End", 4, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/9/9e/Azathoth.png/'],
"Cthulhu": ["Call of Cthulhu", 9, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/9/9e/Cthulhu.png/'],
"Hastur": ["The King in Yellow", 16, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/2/27/Hastur.png/'],
"Hypnos": ["Otherworldly Dreams", 25, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/7/71/Hypnos.png'],
"Ithaqua": ["Rumors from the North", 1, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/1/16/Ithaqua.png/'],
"Nephren-Ka": ["Under the Pyramids", 12, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/c/c9/Nephren-Ka.png/'],
"Nyarlathotep": ["Harbinger of the Outer Gods", 35, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/8/89/Nyarlathotep.png/'],
"Rise of the Elder Things": ["Doomsayer from Antarctica", 5, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/6/62/Elder_Things.png/'],
"Shub-Niggurath": ["Twin Blasphemies of the Black Goat", 23, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/7/70/Shub-Niggurath.png/'],
"Shudde M'ell": ["Apocalypse Nigh", 27, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/0/0e/Shudde_Mell.png/'],
"Syzygy": ["In Cosmic Alignment", 20, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/9/9c/Syzygy.png/'],
"Yig": ["Father of Serpents", 34, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/7/7f/Yig.png/'],
"Yog-Sothoth": ["The Dunwich Horror", 19, 'https://static.wikia.nocookie.net/eldritchhorrorgame/images/4/45/Yog-Sothoth.png/']}

RAW_LIST_OF_PRELUDES = ["Rumors from the North",
    "Key to Salvation", "Unwilling Sacrifice",
    "Beginning of the End", "Doomsayer from Antarctica",
    "Ultimate Sacrifice", "Ghost from The Past",
    "Drastic Measures", "Call of Cthulhu",
    "Litany of Secrets", "Epidemic",
    "Under the Pyramids", "Weakness to Strength",
    "Sins of the Past", "Silver Twilight Stockpile",
    "The King in Yellow", "Dark Blessings",
    "The Coming Storm", "The Dunwich Horror",
    "In Cosmic Alignment", "Written in the Stars",
    "Lurker Among Us", "Twin Blasphemies of the Black Goat",
    "Web Between Worlds", "Otherworldly Dreams",
    "Focused Training", "Apocalypse Nigh",
    "Fall of Man", "The Price of Prestige",
    "You Know What You Must Do", "Aid of the Elder Gods",
    "The Archives", "Army of Darkness",
    "Father of Serpents", "Harbinger of the Outer Gods",
    "In the Lightless Chamber", "The Stars Align",
    "Temptation", "Unto the Breach", "Wondrous Curios"]

def seed_preludes():
    for prelude in RAW_LIST_OF_PRELUDES:
        prelude_obj = Prelude(name=prelude,
                              basic_effect='Basic prelude effect (not native)',
                              special_effect='Special prelude effect (native)')
        db.session.add(prelude_obj)
        db.session.commit()

def seed_ancients():
    for k, v in ANCIENTS_DICT.items():
        ancient_obj = Ancient(name=k,
                              prelude_id=v[1],
                              image_url=v[2])
        db.session.add(ancient_obj)
        db.session.commit()

def download_ancient_images():
    folder_path = os.path.abspath(__file__).replace('db_seeding.py','eldritch_chronicle/static/images/ancients/')
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)

    for k,v in ANCIENTS_DICT.items():
        # make record of path in DB
        ancient = Ancient.query.filter_by(name=k).first()
        ancient.image_path = f"/images/ancients/{'_'.join(k.split(' '))}.png"
        db.session.commit()

        response = requests.get(v[2])

        with open(os.path.join(folder_path, '_'.join(k.split(' '))) + '.png', 'wb') as f:
            f.write(response.content)

def download_investigators_images():
    folder_path = os.path.abspath(__file__).replace('db_seeding.py','eldritch_chronicle/static/images/investigators/')
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)

    for name in INVESTIGATOR_NAMES:

        investigator_page_resp = requests.get('https://eldritchhorror.fandom.com/wiki/' + name)
        investigator_page = BeautifulSoup(investigator_page_resp.content, 'html.parser')
        image_url = investigator_page.select_one(".pi-image-thumbnail")["src"]

        investigator_quest_resp = requests.get("https://eldritchhorror.fandom.com" + \
                              investigator_page.find("h3", text="PERSONAL STORY").findParent("div").findChildren("a")[0].attrs['href'])
        investigator_quest_page = BeautifulSoup(investigator_quest_resp.content, "html.parser")
        quest_source = investigator_quest_page.find("span", text="Personal Mission").findParent("div").findChildren("ul")
        # quest_text = " ".join([elem.get_text() for elem in quest_source[0].children]).strip().replace("\n", "")
        # quest_success = " ".join([elem.get_text() for elem in quest_source[1].children]).strip().replace("\n", "")
        # quest_fail = " ".join([elem.get_text() for elem in quest_source[2].children]).strip().replace("\n", "")
        quest_text = get_quest_data(quest_source[0])
        quest_success = get_quest_data(quest_source[1])
        quest_fail = get_quest_data(quest_source[2])

        image_resp = requests.get(image_url)

        with open(os.path.join(folder_path, name) + '.png', 'wb') as f:
            f.write(image_resp.content)
        #
        investigator = Investigator(name=" ".join(name.split("_")),
                                    personal_quest_text=quest_text,
                                    personal_quest_success=quest_success,
                                    personal_quest_fail=quest_fail,
                                    image_file=f"/images/investigators/{name}.png")
        db.session.add(investigator)
        db.session.commit()

def get_quest_data(parent):
    text = ""
    for item in parent.findChildren("li"):
        text += " • "
        for i in item.descendants:
            try:
                text += f" {i['alt']}" if i['alt'] != "Clue" else ""
            except:
                t = i.get_text()
                if t.lower().replace(" ", "") not in text.lower().replace(" ", ""):
                    text += i.get_text()
    text = text.strip().replace("  ", " ")
    return text

INVESTIGATOR_NAMES = [
    "Agatha_Crane",
    "Agnes_Baker",
    "Akachi_Onyele",
    "Amanda_Sharpe",
    "\"Ashcan\"_Pete",
    "Bob_Jenkins",
    "Calvin_Wright",
    "Carolyn_Fern",
    "Carson_Sinclair",
    "Charlie_Kane",
    "Daisy_Walker",
    "Daniela_Reyes",
    "Darrell_Simmons",
    "Dexter_Drake",
    "Diana_Stanley",
    "Father_Mateo",
    "Finn_Edwards",
    "George_Barnaby",
    "Gloria_Goldberg",
    "Hank_Samson",
    "Harvey_Walters",
    "Jacqueline_Fine",
    "Jenny_Barnes",
    "Jim_Culver",
    "Joe_Diamond",
    "Kate_Winthrop",
    "Leo_Anderson",
    "Lily_Chen",
    "Lola_Hayes",
    "Luke_Robinson",
    "Mandy_Thompson",
    "Marie_Lambeau",
    "Mark_Harrigan",
    "Michael_McGlen",
    "Minh_Thi_Phan",
    "Monterey_Jack",
    "Norman_Withers",
    "Patrice_Hathaway",
    "Preston_Fairmont",
    "Rex_Murphy",
    "Rita_Young",
    "Roland_Banks",
    "Sefina_Rousseau",
    "Silas_Marsh",
    "Sister_Mary",
    "\"Skids\"_O'Toole",
    "Tommy_Muldoon",
    "Tony_Morgan",
    "Trish_Scarborough",
    "Ursula_Downs",
    "Vincent_Lee",
    "Wendy_Adams",
    "William_Yorick",
    "Wilson_Richards",
    "Zoey_Samaras"
]
