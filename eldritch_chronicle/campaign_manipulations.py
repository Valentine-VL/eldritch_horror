import random
from sqlalchemy import select

from eldritch_chronicle import db
from .models import Campaign, Ancient


def get_object_from_db(stmt):
    objects = db.session.execute(stmt)
    return objects.fetchone()[0]


def get_objects_list_from_db(stmt):
    objects = db.session.execute(stmt)
    return [item for item in objects.scalars()]


class CampaignManipulation:
    def __init__(self) -> None:
        pass

    def get_campaign_by_id(self, campaign_id=None):
        if campaign_id is None:
            raise TypeError
        try:
            stmt = select(Campaign).where(Campaign.id == campaign_id)
            self.campaign = get_object_from_db(stmt)
        except Exception as e:
            raise e

    def get_available_ancients_list(self, initial: bool = False):
        """
        :return: list of available ancients objects
        which were not chosen and not defeated in
        current campaign
        """
        if initial:
            stmt = select(Ancient)
        else:
            next_ancient_id = str(self.campaign.next_ancient.id) + ',' if self.campaign.next_ancient else ''
            current_ancient_id = str(self.campaign.current_ancient.id) + ',' if self.campaign.current_ancient else ''
            ids_of_selected_ancients = current_ancient_id + next_ancient_id
            str_of_excluded_ancients_ids = ids_of_selected_ancients + self.campaign.defeated_ancient_ids
            stmt = select(Ancient).where(Ancient.id.not_in(
                [int(id) for id in str_of_excluded_ancients_ids.split(',')[:-1]]
            ))
        filtered_ancients = get_objects_list_from_db(stmt)
        return filtered_ancients

    def create_campaign(self, user_id):
        """
        Campaign creation always selects 2 ancients
        for the first game.
        """
        campaign = Campaign(user_creator_id=user_id)
        db.session.add(campaign)
        db.session.commit()
        self.campaign = campaign
        campaign.defeated_ancient_ids = ''
        campaign.current_ancient, campaign.next_ancient = random.sample(self.get_available_ancients_list(initial=True), 2)
        db.session.commit()

    def defeat_ancient(self):
        """
        When ancient is defeated new one have to
        be chosen and next assigned (except 5th ancient)
        :return:
        """
        self.campaign.defeated_ancient_ids += str(self.campaign.current_ancient.id) + ','
        self.campaign.current_ancient = self.campaign.next_ancient
        if len(self.campaign.defeated_ancient_ids.split(',')[:-1]) >= 5:
            self.campaign.next_ancient = None
        else:
            self.campaign.next_ancient = random.choice(self.get_available_ancients_list())
        if not self.campaign.current_ancient:
            self.campaign.state = "won"
        db.session.commit()

    def get_defeated_ancients(self):
        list_of_defeated_ancients_ids = [int(id) for id in self.campaign.defeated_ancient_ids.split(',')[:-1]]
        list_of_defeated_ancients_ids.reverse()
        stmt = select(Ancient).where(Ancient.id.in_(list_of_defeated_ancients_ids))
        list_of_defeated_ancients = get_objects_list_from_db(stmt)
        return restore_original_order(list_of_defeated_ancients_ids, list_of_defeated_ancients)

    def lose_campaign(self):
        self.campaign.state = "lost"
        db.session.commit()

def restore_original_order(order, list_to_order):
    restored_order_list = []
    for id in order:
        for item in list_to_order:
            if item.id == id:
                restored_order_list.append(item)
                continue
    return restored_order_list
