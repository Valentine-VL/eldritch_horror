"""Gunicorn *development* config file"""

wsgi_app = "eldritch_chronicle:app"
loglevel = "debug"
workers = 2
bind = "127.0.0.1:5000"
reload = True
accesslog = errorlog = "/var/log/gunicorn/dev.log"
capture_output = True
pidfile = "/var/run/gunicorn/dev.pid"
daemon = True